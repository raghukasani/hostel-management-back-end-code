package com.hotel.hotelmanagementsystem.service;

import com.hotel.hotelmanagementsystem.model.ReservedRooms;
import com.hotel.hotelmanagementsystem.repositories.ReservedRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ReservedRoomService {
    @Autowired
    ReservedRoomRepository reservedRoomRepository;

    public ReservedRooms reserveRoomForGuest(ReservedRooms reservedRooms){
        ReservedRooms reservedRooms1 = reservedRoomRepository.save(reservedRooms);
        return reservedRooms1;
    }
    public ReservedRooms getRoom(String roomNumber){
       List<ReservedRooms> reservedRooms = reservedRoomRepository.findAllReservedRoomsByRoomNumber(roomNumber);
        for (ReservedRooms room:reservedRooms) {
            System.out.println(reservedRooms.toString());
        }
        return null;
    }
}
