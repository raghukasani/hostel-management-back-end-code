package com.hotel.hotelmanagementsystem.service;

import com.hotel.hotelmanagementsystem.model.Room;
import com.hotel.hotelmanagementsystem.model.RoomType;
import com.hotel.hotelmanagementsystem.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class RoomService {

    @Autowired
    RoomRepository roomRepository;

    public Room createRoom(Room room){
        System.out.println(room);
        Room room1 = roomRepository.save(room);
        return room;
    }

    public List<Room> getAllRooms(){
        List<Room> allRooms = new ArrayList<>();
        allRooms = (List<Room>) roomRepository.findAll();
        System.out.println(allRooms);
        return allRooms;
    }

    public  void deleteRoom(int id){
        roomRepository.updateDeletionOfRoom(id);
    }

    public Room getById(int id){
        Room room =  roomRepository.findById(id).orElse(null);
        return room;
    }
    public Room UpdateRoom(Room room){
        System.out.println(room);
        Room room1 = roomRepository.save(room);
        return room1;
    }
    public List<Room> SearchingRooms(boolean avaialability,String roomType){
        List<Room> allRooms = new ArrayList<>();
        allRooms = (List<Room>) roomRepository.findAllRoomsByAvailabilityString(avaialability,roomType);
        System.out.println(allRooms);
        return allRooms;
    }

}
