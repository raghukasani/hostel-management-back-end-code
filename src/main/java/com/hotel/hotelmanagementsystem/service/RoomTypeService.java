package com.hotel.hotelmanagementsystem.service;


import com.hotel.hotelmanagementsystem.model.Room;
import com.hotel.hotelmanagementsystem.model.RoomType;
import com.hotel.hotelmanagementsystem.repositories.RoomTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class RoomTypeService {

    @Autowired
    RoomTypeRepository roomTypeRepository;

    public RoomType createRoomType(RoomType roomType){
        System.out.println(roomType);
        RoomType roomType1 = roomTypeRepository.save(roomType);
        return roomType1;
    }

    public List<RoomType> getAllRoomTypes(){
       List<RoomType> allRoomTypes = new ArrayList<>();
        allRoomTypes = (List<RoomType>) roomTypeRepository.findAll();
        return allRoomTypes;
    }

    public  void deleteRoomType(int id){
        roomTypeRepository.updateDeletionOfRoomType(id);
    }

    public RoomType getById(int id){
      RoomType roomType =  roomTypeRepository.findById(id).orElse(null);
      return roomType;
    }
    public RoomType UpdateRoomType(RoomType roomType){
        System.out.println(roomType);
        RoomType roomType1 = roomTypeRepository.save(roomType);
        return roomType1;
    }

    public List<RoomType> searchAllRoomTypes(String code){
        List<RoomType> allRoomTypes = new ArrayList<>();
        allRoomTypes = (List<RoomType>) roomTypeRepository.findAllByCode(code);
        return allRoomTypes;
    }

}
