package com.hotel.hotelmanagementsystem.service;

import com.hotel.hotelmanagementsystem.model.Beds;
import com.hotel.hotelmanagementsystem.model.Room;
import com.hotel.hotelmanagementsystem.repositories.BedsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BedsService {

    @Autowired
    BedsRepository bedsRepository;

    public Beds createBed(Beds beds){
        System.out.println(beds);
        Beds beds1 = bedsRepository.save(beds);
        return beds1;
    }

    public List<Beds> getAllBeds(){
        List<Beds> allBeds = new ArrayList<>();
        allBeds = (List<Beds>) bedsRepository.findAll();
        return allBeds;
    }

    public  void deleteBed(int id){
        bedsRepository.updateDeletionOfBeds(id);
    }

    public Beds getById(int id){
        Beds bed =  bedsRepository.findById(id).orElse(null);
        return bed;
    }
    public  Beds UpdateBed(Beds  beds){
        System.out.println(beds);
        Beds  bed = bedsRepository.save(beds);
        return bed;
    }
}
