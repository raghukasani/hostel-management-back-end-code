package com.hotel.hotelmanagementsystem.service;

import com.hotel.hotelmanagementsystem.model.BedType;
import com.hotel.hotelmanagementsystem.model.Beds;
import com.hotel.hotelmanagementsystem.repositories.BedTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BedTypeService {

    @Autowired
    BedTypeRepository bedTypeRepository;

    public BedType createBedType(BedType bedType){
        System.out.println(bedType);
        BedType bedType1 = bedTypeRepository.save(bedType);
        return bedType1;
    }

    public List<BedType> getAllBedTypes(){
        List<BedType> allBedTypes = new ArrayList<>();
        allBedTypes = (List<BedType>) bedTypeRepository.findAll();
        return allBedTypes;
    }

    public  void deleteBedType(int id){
        bedTypeRepository.updateDeletionOfBedType(id);
    }

    public BedType getById(int id){
        BedType bedType =  bedTypeRepository.findById(id).orElse(null);
        return bedType;
    }
    public  BedType UpdateBedType(BedType  bedType){
        System.out.println(bedType);
        BedType  bedType1 = bedTypeRepository.save(bedType);
        return bedType1;
    }
}
