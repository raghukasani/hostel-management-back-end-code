package com.hotel.hotelmanagementsystem.repositories;

import com.hotel.hotelmanagementsystem.model.Beds;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface BedsRepository extends CrudRepository<Beds,Integer> {
    @Transactional
    @Modifying
    @Query(value = "update Beds set isDeleted = true where id = ?1")
    void updateDeletionOfBeds(int id);
}
