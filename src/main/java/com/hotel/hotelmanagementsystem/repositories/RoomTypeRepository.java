package com.hotel.hotelmanagementsystem.repositories;

import com.hotel.hotelmanagementsystem.model.RoomType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface RoomTypeRepository extends CrudRepository<RoomType,Integer> {
    @Transactional
    @Modifying
    @Query(value = "update RoomType set isDeleted = true where id = ?1")
    void updateDeletionOfRoomType(int id);

    @Transactional
    @Modifying
    @Query(value = "select r from RoomType r where r.code like %?1%")
    List<RoomType> findAllByCode(String code);

}
