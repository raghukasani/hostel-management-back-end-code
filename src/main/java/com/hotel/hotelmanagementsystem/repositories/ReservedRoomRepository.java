package com.hotel.hotelmanagementsystem.repositories;

import com.hotel.hotelmanagementsystem.model.ReservedRooms;
import com.hotel.hotelmanagementsystem.model.Room;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ReservedRoomRepository extends CrudRepository<ReservedRooms,Integer> {

    @Transactional
    @Modifying
    @Query(value = "select r from ReservedRooms r where r.roomNumber = ?1 ")
    List<ReservedRooms> findAllReservedRoomsByRoomNumber (String roomNumber);
}
