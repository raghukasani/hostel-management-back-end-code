package com.hotel.hotelmanagementsystem.repositories;

import com.hotel.hotelmanagementsystem.model.Room;
import com.hotel.hotelmanagementsystem.model.RoomType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface RoomRepository extends CrudRepository<Room,Integer> {
    @Transactional
    @Modifying
    @Query(value = "update Room set isDeleted = true where id = ?1")
    void updateDeletionOfRoom(int id);

    @Transactional
    @Modifying
    @Query(value = "select r from Room r where r.availability = ?1 and r.roomType like %?2%")
    List<Room> findAllRoomsByAvailabilityString (boolean availability,String roomType );
}
