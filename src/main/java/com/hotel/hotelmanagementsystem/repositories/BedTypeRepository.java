package com.hotel.hotelmanagementsystem.repositories;

import com.hotel.hotelmanagementsystem.model.BedType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface BedTypeRepository extends CrudRepository<BedType,Integer> {

    @Transactional
    @Modifying
    @Query(value = "update BedType set isDeleted = true where id = ?1")
    void updateDeletionOfBedType(int id);
}
