package com.hotel.hotelmanagementsystem.customExceptionHandling;

import lombok.Data;

@Data
public class ExceptionFormat {
    private String message;

    public ExceptionFormat(String message) {
        this.message = message;
    }
}
