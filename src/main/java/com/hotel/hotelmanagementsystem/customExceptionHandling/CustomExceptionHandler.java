package com.hotel.hotelmanagementsystem.customExceptionHandling;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        System.out.println("Hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
        ExceptionFormat exceptionFormat=new ExceptionFormat(ex.getBindingResult().getFieldError().getDefaultMessage());
        return new ResponseEntity<>(exceptionFormat, HttpStatus.BAD_REQUEST);
    }
}
