package com.hotel.hotelmanagementsystem.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@Entity
public class RoomType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(unique = true, length = 50)
    @NotNull(message = "Code should not be null")
    @NotEmpty(message = "Code Should not be Empty")
    private String code;

    @NotNull(message = " Description should not be null")
    @NotEmpty(message = "Description Should not be Empty")
    private String description;

    @NotNull(message = " Bathroom Type should not be null")
    @NotEmpty(message = "Bathroom Type Should not be Empty")
    private String bathroom;

    @Pattern(regexp = "[0-9]*" ,message = "only numbers")
    private int numOfGuests;

    private double defaultPrice;

    public RoomType() {
    }


    public int getId() {
        return this.id;
    }

    public @NotNull(message = "Code should not be null") @NotEmpty(message = "Code Should not be Empty") String getCode() {
        return this.code;
    }

    public @NotNull(message = " Description should not be null") @NotEmpty(message = "Description Should not be Empty") String getDescription() {
        return this.description;
    }

    public @NotNull(message = " Bathroom Type should not be null") @NotEmpty(message = "Bathroom Type Should not be Empty") String getBathroom() {
        return this.bathroom;
    }

    public @Pattern(regexp = "[0-9]", message = "only numbers") int getNumOfGuests() {
        return this.numOfGuests;
    }

    public double getDefaultPrice() {
        return this.defaultPrice;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCode(@NotNull(message = "Code should not be null") @NotEmpty(message = "Code Should not be Empty") String code) {
        this.code = code;
    }

    public void setDescription(@NotNull(message = " Description should not be null") @NotEmpty(message = "Description Should not be Empty") String description) {
        this.description = description;
    }

    public void setBathroom(@NotNull(message = " Bathroom Type should not be null") @NotEmpty(message = "Bathroom Type Should not be Empty") String bathroom) {
        this.bathroom = bathroom;
    }

    public void setNumOfGuests(@Pattern(regexp = "[0-9]", message = "only numbers") int numOfGuests) {
        this.numOfGuests = numOfGuests;
    }

    public void setDefaultPrice(double defaultPrice) {
        this.defaultPrice = defaultPrice;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof RoomType)) return false;
        final RoomType other = (RoomType) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getId() != other.getId()) return false;
        final Object this$code = this.getCode();
        final Object other$code = other.getCode();
        if (this$code == null ? other$code != null : !this$code.equals(other$code)) return false;
        final Object this$description = this.getDescription();
        final Object other$description = other.getDescription();
        if (this$description == null ? other$description != null : !this$description.equals(other$description))
            return false;
        final Object this$bathroom = this.getBathroom();
        final Object other$bathroom = other.getBathroom();
        if (this$bathroom == null ? other$bathroom != null : !this$bathroom.equals(other$bathroom)) return false;
        if (this.getNumOfGuests() != other.getNumOfGuests()) return false;
        if (Double.compare(this.getDefaultPrice(), other.getDefaultPrice()) != 0) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RoomType;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * PRIME + this.getId();
        final Object $code = this.getCode();
        result = result * PRIME + ($code == null ? 43 : $code.hashCode());
        final Object $description = this.getDescription();
        result = result * PRIME + ($description == null ? 43 : $description.hashCode());
        final Object $bathroom = this.getBathroom();
        result = result * PRIME + ($bathroom == null ? 43 : $bathroom.hashCode());
        result = result * PRIME + this.getNumOfGuests();
        final long $defaultPrice = Double.doubleToLongBits(this.getDefaultPrice());
        result = result * PRIME + (int) ($defaultPrice >>> 32 ^ $defaultPrice);
        return result;
    }

    public String toString() {
        return "RoomType(id=" + this.getId() + ", code=" + this.getCode() + ", description=" + this.getDescription() + ", bathroom=" + this.getBathroom() + ", numOfGuests=" + this.getNumOfGuests() + ", defaultPrice=" + this.getDefaultPrice() + ")";
    }
}
