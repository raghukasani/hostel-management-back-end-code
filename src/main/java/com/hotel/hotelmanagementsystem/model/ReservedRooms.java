package com.hotel.hotelmanagementsystem.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class ReservedRooms {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String roomNumber;

    private  String firstName;

    private String lastName;

    private String emailId;

    private String phoneNumber;

    private String fromDate;

    private String toDate;


}
