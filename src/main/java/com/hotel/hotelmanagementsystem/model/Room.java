package com.hotel.hotelmanagementsystem.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
@Entity
@Data
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(unique = true, length = 50)
    @NotNull(message = "Code should not be null")
    @NotEmpty(message = "Code Should not be Empty")
    private String roomNumber;

    @NotNull(message = " Description should not be null")
    @NotEmpty(message = "Description Should not be Empty")
    private String description;

    @NotNull(message = " Room Type should not be null")
    @NotEmpty(message = "Room Type Should not be Empty")
    private String roomType;

    private double area;

    @Min(1)
    private int bedsInRoom;

    private boolean isDeleted;

    @OneToMany(targetEntity = Beds.class,cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<Beds> bedsList = new ArrayList<>();

    private boolean availability;


}