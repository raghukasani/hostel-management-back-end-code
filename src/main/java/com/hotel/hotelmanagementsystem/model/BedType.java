package com.hotel.hotelmanagementsystem.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Data
public class BedType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(unique = true, length = 50)
    @NotNull(message = " Code should not be null")
    @NotEmpty(message = "Code Should not be Empty")
    private String code;

    @NotNull(message = " Description should not be null")
    @NotEmpty(message = "Description Should not be Empty")
    private String description;


    private int numOfPersons;

    @NotNull(message = " Location should not be null")
    @NotEmpty(message = "Location Should not be Empty")
    private String location;

    private boolean isDeleted;


}
