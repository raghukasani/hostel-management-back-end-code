package com.hotel.hotelmanagementsystem.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class Beds {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(unique = true, length = 50)
    @NotNull(message = "Code should not be null")
    @NotEmpty(message = "Code Should not be Empty")
    private String code;

    @NotNull(message = " Description should not be null")
    @NotEmpty(message = "Description Should not be Empty")
    private String descriptions;

    @NotNull(message = " Room should not be null")
    @NotEmpty(message = "Room Should not be Empty")
    private String room;

    @NotNull(message = " Bed Type should not be null")
    @NotEmpty(message = "Bed Type  Should not be Empty")
    private String bedType;

    private boolean isDeleted;

}