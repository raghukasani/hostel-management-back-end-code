package com.hotel.hotelmanagementsystem.controller;

import com.hotel.hotelmanagementsystem.model.*;
import com.hotel.hotelmanagementsystem.repositories.RoomRepository;
import com.hotel.hotelmanagementsystem.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class HotelManagementController {

    @Autowired
    RoomTypeService roomTypeService;

    @Autowired
    RoomService roomService;

    @Autowired
    BedsService bedsService;

    @Autowired
    BedTypeService bedTypeService;

    @Autowired
    ReservedRoomService reservedRoomService;
//********************Room Type Methods**************
    @PostMapping("/roomType")
    public ResponseEntity<RoomType>  addRoomType(@RequestBody @Valid RoomType roomType){
        System.out.println(roomType.toString());
        RoomType roomType1 = roomTypeService.createRoomType(roomType);
        if (roomType1 != null){
           return new ResponseEntity<>(roomType1,HttpStatus.CREATED);
        }
        return null;
    }

    @GetMapping("/roomType")
    public List<RoomType> getAllRoomTypes(){

        return roomTypeService.getAllRoomTypes();
    }
    @DeleteMapping("/roomType/{id}")
    public void deleteRoomType(@PathVariable String id){
        RoomType roomType = roomTypeService.getById(Integer.parseInt(id));
        if(roomType != null){
            roomTypeService.deleteRoomType(Integer.parseInt(id));
        }
    }
    @GetMapping("/roomType/{id}")
    public RoomType getOneRoomType(@PathVariable String id){
        RoomType roomType = roomTypeService.getById(Integer.parseInt(id));
        if(roomType == null){
            return null;
        }else {

            return  roomType;
        }
    }
    @PutMapping("/roomType")
    public RoomType updateRoomType(@RequestBody @Valid RoomType roomType){
    return roomTypeService.UpdateRoomType(roomType);
    }
    //*********************Room methods*********************
    @PostMapping("/room")
    public Room addRoom(@RequestBody @Valid Room room){
        System.out.println(room.toString());
        Room room1 = roomService.createRoom(room);
        return room1;
    }
    @GetMapping("/room")
    public List<Room> getAllRoom(){

        return roomService.getAllRooms();
    }
    @DeleteMapping("/room/{id}")
    public void deleteRoom(@PathVariable String id){
        Room room = roomService.getById(Integer.parseInt(id));
        if(room != null){
            roomService.deleteRoom(Integer.parseInt(id));
        }
    }
    @GetMapping("/room/{id}")
    public Room getOneRoom(@PathVariable String id){
        Room room = roomService.getById(Integer.parseInt(id));
        if(room == null){
            return null;
        }else {

            return  room;
        }
    }
    @PutMapping("/room")
    public Room updateRoom(@RequestBody @Valid Room room){
        return roomService.UpdateRoom(room);
    }

    //****************************Bed Class Methods********************
    @PostMapping("/bed")
    public Beds addBed(@RequestBody @Valid Beds bed){
        System.out.println(bed.toString());
        Beds bed1 = bedsService.createBed(bed);
        return bed1;
    }
    @GetMapping("/bed")
    public List<Beds> getAllBeds(){

        return bedsService.getAllBeds();
    }
    @DeleteMapping("/bed/{id}")
    public void deleteBed(@PathVariable String id){
        Beds bed = bedsService.getById(Integer.parseInt(id));
         if(bed!=null){
            bedsService.deleteBed(Integer.parseInt(id));
          }
    }
    @GetMapping("/bed/{id}")
    public Beds getOneBed(@PathVariable String id){
        Beds bed = bedsService.getById(Integer.parseInt(id));
        if(bed == null){
            return null;
        }else {

            return  bed;
        }
    }
    @PutMapping("/bed")
    public Beds updateBed(@RequestBody@Valid Beds bed){
        return bedsService.UpdateBed(bed);
    }

    //******************** Bed Type service Methods ******************************

    @PostMapping("/bedType")
    public BedType addBedType(@RequestBody @Valid BedType bedType){
        System.out.println(bedType.toString());
        BedType bedType1 = bedTypeService.createBedType(bedType);
        return bedType1;
    }
    @GetMapping("/bedType")
    public List<BedType> getAllBedTypes(){

        return bedTypeService.getAllBedTypes();
    }
    @DeleteMapping("/bedType/{id}")
    public void deleteBedType(@PathVariable String id){
        BedType bedType = bedTypeService.getById(Integer.parseInt(id));
        if(bedType!=null){
            bedTypeService.deleteBedType(Integer.parseInt(id));
        }
    }
    @GetMapping("/bedType/{id}")
    public BedType getOneBedType(@PathVariable String id){
        BedType bedType = bedTypeService.getById(Integer.parseInt(id));
        if(bedType == null){
            return null;
        }else {

            return  bedType;
        }
    }
    @PutMapping("/bedType")
    public BedType updateBedType(@RequestBody @Valid BedType bedType){
        return bedTypeService.UpdateBedType(bedType);
    }
//******************************** Enquiry  ******************************************************
    @GetMapping("/roomTypes/by/code/{code}")
    public List<RoomType> getAllRoomTypesByCode(@PathVariable String code){
        return roomTypeService.searchAllRoomTypes(code);
    }

    @GetMapping("/rooms/by/availability/{availability}/{roomType}")
    public List<Room> getAllRoomTypesByCode(@PathVariable String availability,@PathVariable String roomType ){
        return roomService.SearchingRooms(Boolean.parseBoolean(availability),roomType);
    }
//************************** Reserve ***********************
@PostMapping("/reserveRoom")
public ReservedRooms blockRoom(@RequestBody ReservedRooms reservedRooms){
    System.out.println();
      return this.reservedRoomService.reserveRoomForGuest(reservedRooms);

}

}
